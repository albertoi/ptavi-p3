#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom

def main():
    """Programa principal"""
    document = xml.dom.minidom.parse('chistes.xml')
    jokes = document.getElementsByTagName('chiste')
    for joke in jokes:
        score = joke.getAttribute('calificacion')
        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        print(f"Calificación: {score}.")
        print(f" Pregunta: {question}")
        print(f" Respuesta: {answer}")



if __name__ == "__main__":
    main()
