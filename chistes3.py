#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

from xml.dom.minidom import parse


def muestra(i, score):
    questions = i.getElementsByTagName('pregunta')
    question = questions[0].firstChild.nodeValue.strip()
    answers = i.getElementsByTagName('respuesta')
    answer = answers[0].firstChild.nodeValue.strip()
    print(f"Calificación: {score}.")
    print(f" Pregunta: {question}")
    print(f" Respuesta: {answer}")
    print("                      ")
def main(path):
    """Programa principal"""
    document = parse(path)

    if document.getElementsByTagName('humor'):
        jokes = document.getElementsByTagName('chiste')
        ChistesMalos = []
        ChistesMalisimos = []
        ChistesNormales = []
        ChistesBuenisimos = []
        ChistesBuenos = []
        for joke in jokes:
            score = joke.getAttribute('calificacion')
            if score == "buenisimo":
                ChistesBuenisimos.append(joke)
            if score == "bueno":
                ChistesBuenos.append(joke)
            if score == "regular":
                ChistesNormales.append(joke)
            if score == "malo":
                ChistesMalos.append(joke)
            if score == "malisimo":
                ChistesMalisimos.append(joke)
        for i in ChistesBuenisimos:
            muestra(i,"Buenisimo")
        for i in ChistesBuenos:
            muestra(i, "Buenos")
        for i in ChistesNormales:
            muestra(i, "regular")
        for i in ChistesMalos:
            muestra(i, "malo")
        for i in ChistesMalisimos:
            muestra(i, "malisimo")

    else:
        print("Root Element is not humor")


if __name__ == "__main__":
    main("chistes.xml")
