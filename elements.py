import smil
import sys


def main(path=None):
    if path == None:
        print("Utiliza: python3 elements.py <file>")
    else:
        archivo = smil.SMIL(path)
        for i in archivo.elements():
            print(i.name())


if __name__ == "__main__":
    main(sys.argv[1])
